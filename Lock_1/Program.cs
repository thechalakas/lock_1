﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Lock_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //a value to which I will synchronize access between threads

            int temp = 0;

            //creating the lock object
            object lock_temp = new object();

            //for loop that will increment temp some 1000000 times. so it becomes hundred
            for(int i=0;i< 1000000; i++)
            {
                //this thread and temp is now locked. until the thread completes, others cannot touch it
                lock (lock_temp) temp++;
            }

            Console.WriteLine("Value of temp is {0}", temp);

            //another thread that will decrement temp some 100 times so it becomes zero.
            //in this thread I will use a lock to indicate that the other threads should wait for this to finish
            Task second_thread = Task.Run(() =>
            {
                for (int i = 0; i < 1000000; i++)
                {
                    //this thread and temp is now locked. until the thread completes, others cannot touch it
                    lock (lock_temp) temp--;
                }
            }
                );

            //starting the second thread
            second_thread.Wait();

            Console.WriteLine("Value of temp is {0}", temp);



            //dont let the console window not dissapear
            Console.ReadLine();
        }
    }
}
